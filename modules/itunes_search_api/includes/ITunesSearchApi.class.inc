<?php

class ITunesSearchApi extends ExternalApiCache {
  // the url to the API endpoint
  const REQUEST_URL = 'https://itunes.apple.com/';

  // cache prefix, for namespacing
  const PREFIX = 'itunesapi';

  // cache lifetime, as would be passed to strtotime()
  const CACHE_TIME = '+1 day';

  // the affiliate id for linking to the iTunes store
  const AFFILIATE_ID = NULL;

  // allowed operations as defined by the api
  private $ops = array(
    'search' => array(
      'url' => 'search',
      'accept' => 'application/json',
      'parameters' => array(
        'term' => NULL,
        'country' => 'CA',
        'media' => NULL,
        'entity' => NULL,
        'attribute' => NULL,
        'callback' => NULL,
        'limit' => NULL,
        'lang' => 'en_us',
        'version' => NULL,
        'explicit' => NULL,
      ),
    ),
    'lookup' => array(
      'url' => 'lookup',
      'accept' => 'application/json',
      'parameters' => array(
        'id' => NULL,
        'isbn' => NULL,
        'amgAlbumId' => NULL,
        'amgArtistId' => NULL,
        'amgVideoId' => NULL,
        'upc' => NULL,
        'country' => 'CA',
        'media' => NULL,
        'entity' => NULL,
        'attribute' => NULL,
        'callback' => NULL,
        'limit' => NULL,
        'lang' => 'en_us',
        'version' => NULL,
        'explicit' => NULL,
      ),
    ),
  );

  /**
   * Build a new ITunesSearchAPI object to execute cached queries against the
   * iTunes Search API.
   *
   * @param string $op
   *   The operation to execute against to API.
   */
  public function __construct($op) {
    // check for a supported operation
    if (!isset($this->ops[$op])) {
      throw new Exception('iTunes Search API unsupported operation "' . $op . '"');
    }

    // set the cache lifetime and prefix for this API
    parent::setLifetime(self::CACHE_TIME);
    parent::setPrefix(self::PREFIX);

    // set the operation
    $this->op = $op;
  }

  /**
   * Sets the relevant parameter for this request, throws an error if the
   * parameter is not supported.
   *
   * This function suuports method chaining.
   *
   * @param string $key
   *   The parameter to set.
   * @param string $value
   *   The value of the parameter.
   */
  public function setParameter($key, $value) {
    // check the parameter key is valid
    if (!array_key_exists($key, $this->ops[$this->op]['parameters'])) {
      throw new Exception('iTunes Search API unsupported parameter for operation "' . $key . '"');
    }

    // set the parameter
    $this->ops[$this->op]['parameters'][$key] = $value;
    return $this;
  }

  /**
   * Sets multiple parameters for this request, values should be passed in as
   * key => value pairs in an associative array.
   *
   * This function suuports method chaining.
   *
   * @param array $params
   *   An associative array of parameters to set.
   */
  public function setParameters($params) {
    // loop over array
    foreach ($params as $key => $value) {
      // check the parameter key is valid
      if (!array_key_exists($key, $this->ops[$this->op]['parameters'])) {
        throw new Exception('iTunes Search API unsupported parameter for operation "' . $key . '"');
      }

      // set the parameter
      $this->ops[$this->op]['parameters'][$key] = $value;
    }

    return $this;
  }

  /**
   * Returns the parameter string for this request, some parameters can be
   * arrays, so we need to collapse those (and http_build_query isn't helpful),
   * this is an internal helper function.
   *
   * @return string
   *   The query string.
   */
  public function getParameterString() {
    $params = array();
    // loop over possible parameters
    foreach ($this->ops[$this->op]['parameters'] AS $key => $value) {
      // no need for null values
      if (isset($value)) {
        $params[] = $key . '=' . $value;
      }
    }
    // create query string
    return implode('&', $params);
  }

  /**
   * Attempts to load data from the cache first, then accesses the api as a
   * fallback, returning the full response from drupal_http_request, with the
   * data variable parsed out from JSON.
   *
   * This function does not throw errors on 404, 405, etc, that is up to the
   * implementing script to handle.
   *
   * @return response
   *   The full api response.
   */
  public function execute() {
    // build query
    $query = $this->getParameterString();
    $url = self::REQUEST_URL . $this->ops[$this->op]['url'] . '?' . $query;

    // attempt to pull from the cache
    $request = &parent::getCache($url);

    // data is not cached yet, proceed with API call
    if (!isset($request)) {
      // generic options for this API
      $options = array(
        'method' => 'GET',
        'headers' => array(
          'Accept' => $this->ops[$this->op]['accept'],
        ),
      );

      $request = drupal_http_request($url, $options);

      // reponse was good, parse the data
      if ($request->code == 200) {
        // decode json requests
        if ($this->ops[$this->op]['accept'] == 'application/json') {
          $request->data = json_decode($request->data);

          // append affiliate url
          foreach ($request->data->results as &$result) {
            if (isset($result->collectionViewUrl))
              $result->affiliateUrl = $result->collectionViewUrl . '&at=' . self::AFFILIATE_ID;
            else if (isset($result->trackViewUrl))
              $result->affiliateUrl = $result->trackViewUrl . '&at=' . self::AFFILIATE_ID;
          }
        }

        // cache the reponse
        parent::setCache($url, $request);
      }
    }

    // return the (now at least statically cached) request
    return $request;
  }
}
