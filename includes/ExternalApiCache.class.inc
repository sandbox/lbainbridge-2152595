<?php

abstract class ExternalApiCache {
  /**
   * Abstract function, any classes implementing the APICache class should
   * call getCache to check the cache and expect a null return if not set, then
   * call setCache to put items in the drupal cache within their execute()
   * function.  Static caching takes place automatically after calling the
   * getCache() function.
   */
  abstract public function execute();

  // cache lifetime default
  public $lifetime = '+1 month';

  // this apis prefix
  public $prefix;

  /**
   * Gets data from the cache, attempting the static cache first.
   *
   * @param  string $request
   *   The url being requested, used to provide a unique cid.
   * @return mixed
   *   The response from the cache, if one exists.
   */
  public function &getCache($request) {
    $cid = $this->prefix . ':' . $request;
    $response = &drupal_static($cid);
    if (!isset($response)) {
      if ($cache = cache_get($cid, EXTERNAL_API_CACHE_BIN)) {
        $response = $cache->data;
      }
    }
    return $response;
  }

  /**
   * Saves data to drupal's cache using the default drupal implementation.
   *
   * @param string $request
   *   The url being requested, used to provide a unique cid.
   * @param mixed  $data
   *   The data to store in the cache.
   */
  public function setCache($request, $data) {
    $cid = $this->prefix . ':' . $request;
    cache_set($cid, $data, EXTERNAL_API_CACHE_BIN, strtotime($this->lifetime));
  }

  /**
   * Setter function for the lifetime variable.
   *
   * @param string $lifetime
   *   The lifetime string, will be passed to strtotime().
   */
  public function setLifetime($lifetime) {
    $this->lifetime = $lifetime;
  }

  /**
   * Setter function for the prefix variable.
   *
   * @param string $prefix
   *   The prefix for this api, to help namespace the cache_bin table.
   */
  public function setPrefix($prefix) {
    $this->prefix = $prefix;
  }
}
